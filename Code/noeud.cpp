#include "noeud.hpp"

Noeud::Noeud(int v) :
  gauche(nullptr),
  droite(nullptr),
  valeur(v),
  hauteur(0)
{}

Noeud::~Noeud() {
  delete gauche ;
  delete droite ;
}

